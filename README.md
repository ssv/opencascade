# OpenCascade vanilla

This is a fork from OpenCascade 7.4.0. It's main idea is to stay compatible with the original 7.4.0 version, so it does not bring any new features or major bug corrections. The necessity of this fork is justified here: http://quaoar.su/blog/page/meet-analysis-situs-11
